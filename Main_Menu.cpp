/*******************************************************************************************************************

                          Create a Program with multiple playing games:
a) Random number guessing game. Program will ask user a number and check weather it is equal to number generated by 
random function between 1 to 100.
b) Tic Tac Toe using multidimention array.

*********************************************************************************************************************/

#include <iostream>

using namespace std;

int main()
{
    int choice=0;
    cout<<" \n\t******************* Main Menu *******************"<<endl;
    cout<<"\n\n";
    cout<<" \t a) Random Number game "<<endl;
    cout<<" \t b) Tic Tac Toe "<<endl;
    cout<<"\n";
    cout<<" Select an option: ";
    cin>>choice;
    //system ("pause");
    //system ("cls")

    return 0;
}
